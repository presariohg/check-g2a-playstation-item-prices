#TODO: them cai so sanh gia (save file, lich su, json). fix sorting
import requests
import bs4 as bs
import re
import json
from time import gmtime, strftime

PRICE_PSN_30    =   8
PRICE_PSN_90    =   25
PRICE_PSN_365   =   60
PRICE_PSN_15M   =   75

PRICE_NOT_DEFINED = -1

def get_raw_soup():
    rq      =   requests.Session()
    params  =   {
                    'query'     :   'playstation germany',
                    'sort'      :   'price-highest-first',
                }
    headers =   {
                    'accept'                    :   'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding'           :   'gzip, deflate, br',
                    'accept-language'           :   'en-US,en;q=0.9,vi;q=0.8,de;q=0.7',
                    'cache-control'             :   'max-age=0',
                    'dnt'                       :   '1',
                    'upgrade-insecure-requests' :   '1',
                    'user-agent'                :   'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                }

    page    =   rq.get('https://www.g2a.com/de-de/search', params = params, headers = headers)

    with open('out.html', 'w') as f:
        f.write(page.text)


    return  bs.BeautifulSoup(page.text, 'lxml')


def draw_table():
    print(f"╔{'':═^54}╦{'':═^8}╦{'':═^9}╦{'':═^9}╦{'':═^7}╗")
    print(f"║{'Article Name':^54}║ Origin ║ Offered ║ Balance ║ Worth ║")
    print(f"╠{'':═^54}╬{'':═^8}╬{'':═^9}╬{'':═^9}╬{'':═^7}╣")

    for item in article_list:
        if (item['price_predefined']):
            print(f"║  {item['name']:<52}║{item['price_original']:^8}║{item['price_reduced']:^9.2f}║{item['balance']:^9.2%}║{item['evaluation']:^7}║")
        else:
            print(f"║  {item['name']:<52}║  N/A   ║{item['price_reduced']:^9.2f}║   ---   ║       ║")

    print(f"╚{'':═^54}╩{'':═^8}╩{'':═^9}╩{'':═^9}╩{'':═^7}╝")
    print(strftime("%H:%M:%S ║ %d/%m/%Y", gmtime()))


soup            =   get_raw_soup()
name_list       =   soup.find_all(class_ = 'Card__title')
price_list      =   soup.find_all(class_ = 'Card__price-cost price')

price_original  =   []
price_reduced   =   []
article_list    =   []

for i in range(len(name_list)):
    name_list[i]    =   name_list[i].text
    article_name    =   name_list[i].casefold()

    try:
        price       =   re.findall(r' (\d+?) eur', article_name)[0]
    except IndexError:
        if ('15 months' in article_name):
            price   =   PRICE_PSN_15M
        elif ('30 days' in article_name):
            price   =   PRICE_PSN_30
        elif ('90 days' in article_name):
            price   =   PRICE_PSN_90
        elif ('365 days'  in article_name):
            price   =   PRICE_PSN_365
        else:
            price   =   PRICE_NOT_DEFINED
        
    price_original.append(int(price))
    
for i in range(len(price_list)):
    price       =   re.findall(r'(\d+?\.\d+?) eur', price_list[i].text, re.IGNORECASE)[0]
    price_reduced.append(float(price))

for i in range(len(price_list)):

    if (price_original[i] != PRICE_NOT_DEFINED):
        balance         =   price_reduced[i]/price_original[i]
        evaluation      =   '✔' if (balance <= 0.72) else ' '

        article_list.append({
            'name'              :   name_list[i],
            'price_original'    :   price_original[i],
            'price_reduced'     :   price_reduced[i],
            'balance'           :   balance,
            'evaluation'        :   evaluation,
            'time'              :   strftime("%H:%M:%S ║ %d/%m/%Y",gmtime()),
            'price_predefined'  :   True
            })
    else: 
        article_list.append({
            'name'              :   name_list[i],
            'price_original'    :   price_original[i],
            'price_reduced'     :   price_reduced[i],
            'time'              :   strftime("%H:%M:%S ║ %d/%m/%Y",gmtime()),
            'price_predefined'  :   False
            })


draw_table()

with open('foo.json', 'w') as f:
    f.write(json.dumps(article_list, ensure_ascii = False, indent = 4))
