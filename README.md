Sample:
```
╔════════════════════════════════════════════════════╦════════╦═════════╦═════════╦═══════╗
║                    Article Name                    ║ Origin ║ Reduced ║ Balance ║ Worth ║
╠════════════════════════════════════════════════════╬════════╬═════════╬═════════╬═══════╣
║  PlayStation Network Gift Card 75 EUR PSN GERMANY  ║   75   ║  59.88  ║ 79.84%  ║       ║
║  Playstation Plus CARD PSN GERMANY 365 Days        ║   60   ║  50.98  ║ 84.97%  ║       ║
║  PlayStation Network Gift Card 50 EUR PSN GERMANY  ║   50   ║  34.92  ║ 69.84%  ║   X   ║
║  PlayStation Network Gift Card 40 EUR PSN GERMANY  ║   40   ║  31.00  ║ 77.50%  ║       ║
║  PlayStation Network Gift Card 35 EUR PSN GERMANY  ║   35   ║  24.99  ║ 71.40%  ║   X   ║
║  PlayStation Network Gift Card 30 EUR PSN GERMANY  ║   30   ║  23.89  ║ 79.63%  ║       ║
║  Playstation Plus CARD PSN GERMANY 90 Days         ║   25   ║  22.24  ║ 88.96%  ║       ║
║  PlayStation Network Gift Card 25 EUR PSN GERMANY  ║   25   ║  19.98  ║ 79.92%  ║       ║
║  PlayStation Network Gift Card 20 EUR PSN GERMANY  ║   20   ║  18.40  ║ 92.00%  ║       ║
║  PlayStation Network Gift Card 15 EUR PSN GERMANY  ║   15   ║  11.99  ║ 79.93%  ║       ║
║  PlayStation Network Gift Card 10 EUR PSN GERMANY  ║   10   ║  8.92   ║ 89.20%  ║       ║
║  PlayStation Network Gift Card 10 EUR PSN GERMANY  ║   10   ║  8.50   ║ 85.00%  ║       ║
║  PlayStation Network Gift Card 5 EUR PSN GERMANY   ║   5    ║  4.18   ║ 83.60%  ║       ║
╚════════════════════════════════════════════════════╩════════╩═════════╩═════════╩═══════╝

14:17:10 ║ 15/12/2018
```